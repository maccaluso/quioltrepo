import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
/*
	Generated class for the DataProvider provider.

	See https://angular.io/guide/dependency-injection for more info on providers
	and Angular DI.
*/
@Injectable()
export class DataProvider {

	/////////////////////////////////
	// vecchi path per dati locali //
	/////////////////////////////////
	// private argumentsEndpoint = './assets/data.json';
	// private townsEndpoint = './assets/towns.json';
	// private routesEndpoint = './assets/routes.json';

	/////////////////////////////////////////
	// decommentare per lavorare in locale //
	/////////////////////////////////////////
	// private argumentsEndpoint = '/api/arguments';
	// private townsEndpoint = '/api/towns';
	// private routesEndpoint = '/api/customroute';

	/////////////////////////////////////////////////////////
	// decommentare per lavorare in produzione o su device //
	/////////////////////////////////////////////////////////
	private argumentsEndpoint = 'https://www.itinerarioltrepomantovano.it/api/arguments';
	private townsEndpoint = 'https://www.itinerarioltrepomantovano.it/api/towns';
	private routesEndpoint = 'https://www.itinerarioltrepomantovano.it/api/customroute';

	constructor(public http: HttpClient) {
		// console.log('Hello DataProvider Provider');
	}

	getArguments(): Observable<string[]> {
		return this.http.get(this.argumentsEndpoint)
										.catch(this.handleError);
	}

	getTowns(): Observable<string[]> {
		return this.http.get(this.townsEndpoint)
										.catch(this.handleError);
	}

	getRoutes(routeCode): Observable<string[]> {
		return this.http.get(this.routesEndpoint + '/' + routeCode)
										.catch(this.handleError);
										// return this.http.get(this.routesEndpoint)
																		// .catch(this.handleError);
	}


	private handleError (error: Response | any) {
		let errMsg: string;
		if (error instanceof Response) {
			const err = error || '';
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		console.error(errMsg);
		return Observable.throw(errMsg);
	}

}
