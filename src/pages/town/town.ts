import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TranslateService } from '@ngx-translate/core';
import { DataProvider } from '../../providers/data/data';

@IonicPage()
@Component({
  selector: 'page-town',
  templateUrl: 'town.html',
})
export class TownPage {
	item: any;

  arguments: any[];
  towns: any[];
  errorMessage: string;
  lang: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, private iab: InAppBrowser,private translate: TranslateService) {
  	this.item = navParams.get('town') || {};
    translate.onLangChange.subscribe(event => {
      this.lang = event.lang;
  });
  }

  ionViewDidLoad() {
    this.getTowns();
    this.getArguments();
    this.lang = this.translate.currentLang;
  
  }

  getTowns() {
    this.data.getTowns()
      .subscribe(
         townList => this.towns = townList,
         error =>  this.errorMessage = <any>error
      );
  }

  getArguments() {
    this.data.getArguments()
      .subscribe(
         argumentList => this.arguments = argumentList,
         error =>  this.errorMessage = <any>error
      );

  }



  goBack() { this.navCtrl.pop(); }

  goArgomento(a) {
    let argument;
    for(let i in this.arguments)
    {
      if( this.arguments[i]._id == a )
        argument = this.arguments[i];
    }

    this.navCtrl.push( 'ArgumentPage', { arg: argument } );
  }

  goComune(t) {
    let town;
    for(let i in this.towns)
    {
      if( this.towns[i]._id == t )
        town = this.towns[i];
    }

    this.navCtrl.push( 'TownPage', { town: town } );
  }

  openInBrowser(lat, lng) {
    this.iab.create('https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
  }

}
