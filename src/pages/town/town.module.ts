import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { TranslateModule } from '@ngx-translate/core';
import { TownPage } from './town';

@NgModule({
  declarations: [
    TownPage,
  ],
  imports: [
  	CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'itinerarioltrepomantovano' } as CloudinaryConfiguration),
    IonicPageModule.forChild(TownPage),
    TranslateModule
  ],
})
export class TownPageModule {}
