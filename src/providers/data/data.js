var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
/*
    Generated class for the DataProvider provider.

    See https://angular.io/guide/dependency-injection for more info on providers
    and Angular DI.
*/
var DataProvider = /** @class */ (function () {
    function DataProvider(http) {
        this.http = http;
        // private apiUrl = 'https://restcountries.eu/rest/v2/all';
        this.argumentsEndpoint = './assets/data.json';
        this.townsEndpoint = './assets/towns.json';
        console.log('Hello DataProvider Provider');
    }
    DataProvider.prototype.getArguments = function () {
        return this.http.get(this.argumentsEndpoint)
            .catch(this.handleError);
    };
    DataProvider.prototype.getTowns = function () {
        return this.http.get(this.townsEndpoint)
            .catch(this.handleError);
    };
    // private extractData(res: Response) {
    // 	let body = res;
    // 	return body || { };
    // }
    DataProvider.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var err = error || '';
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    DataProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], DataProvider);
    return DataProvider;
}());
export { DataProvider };
//# sourceMappingURL=data.js.map