import {Pipe} from '@angular/core';

@Pipe({name: "sortBy"})
export class SortDistancePipe {

  transform(array: Array<any>, args: string): Array<string> {



if (array !== undefined && args != 'isFeatured') {

    array.sort((a: any, b: any) => {
	    if ( a[args] < b[args] ){
	    	return -1;
	    }else if( a[args] > b[args] ){
	        return 1;
	    }else{
	    	return 0;
	    }
    });
    console.log(array.length)
    if(args == 'distanceUser'){
      for (let i = 0;i<array.length;i++){
        // console.log(array[i].distanceUser)
        if(array[i].distanceUser > 50000){

          array.splice(i, 1);

        }
      }
      console.log(array.length)
    }
    return array;
  }
  else if(array !== undefined && args == 'isFeatured'){

    var arrayRet = new Array();
    for (let i = 0;i<array.length;i++){

      if (array[i].isFeatured){

        arrayRet.push(array[i]);
      }
    }
    console.log(arrayRet)
    return arrayRet;
  }
}
}
