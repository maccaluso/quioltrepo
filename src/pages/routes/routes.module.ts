import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { RoutesPage } from './routes';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    RoutesPage
  ],
  imports: [
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'itinerarioltrepomantovano' } as CloudinaryConfiguration),
    IonicPageModule.forChild(RoutesPage),
    TranslateModule
  ]
})
export class RoutesPageModule {}
