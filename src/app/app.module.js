var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
// import { Cloudinary as CloudinaryCore } from 'cloudinary-core';
// import { CloudinaryConfiguration, CloudinaryModule } from '@cloudinary/angular-5.x';
// import cloudinaryConfiguration from './config';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ModalContentPage } from '../pages/home/home';
// import { ArgumentPage } from '../pages/argument/argument';
import { DataProvider } from '../providers/data/data';
// import { Cloudinary } from '@cloudinary/angular-5.x/src/cloudinary.service';
// export const cloudinary = {
//   Cloudinary: CloudinaryCore
// };
// export const config: CloudinaryConfiguration = cloudinaryConfiguration;
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                HomePage,
ModalContentPage
            ],
            imports: [
                BrowserModule,
                CloudinaryModule.forRoot({ Cloudinary: Cloudinary }, { cloud_name: 'itinerarioltrepomantovano' }),
                // CloudinaryModule.forRoot(cloudinary, config),
                HttpModule,
                HttpClientModule,
                IonicModule.forRoot(MyApp, {
                    pageTransition: 'ios-transition'
                })
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                HomePage,
ModalContentPage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                DataProvider
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map
