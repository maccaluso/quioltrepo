import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DataProvider } from '../../providers/data/data';
import { TranslateService } from '@ngx-translate/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
/**
 * Generated class for the ArgumentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-argument',
  templateUrl: 'argument.html',
})
export class ArgumentPage {
	item: any;
  blocks: any[] = Array();
  arguments: any[];
  towns: any[];
  errorMessage: string;
  lang: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider, private iab: InAppBrowser,private translate: TranslateService) {
  	this.item = navParams.get('arg') || {};
  	this.blocks.push( this.item.block1 );
    this.blocks.push( this.item.block2 );
    this.blocks.push( this.item.block3 );
    this.blocks.push( this.item.block4 );
    this.blocks.push( this.item.block5 );

    translate.onLangChange.subscribe(event => {
      this.lang = event.lang;
  });
  }

  ionViewDidLoad() {
    this.getArguments();
    this.getTowns();
    this.lang = this.translate.currentLang;
  }

  getArguments() {
    this.data.getArguments()
      .subscribe(
         argumentList => this.arguments = argumentList,
         error =>  this.errorMessage = <any>error
      );
  }



  getTowns() {
    this.data.getTowns()
      .subscribe(
         townList => this.towns = townList,
         error =>  this.errorMessage = <any>error
      );
  }

  getRelatedTown(id){

    var notFinded = true;
    if(id != null){

      for(let t in this.towns){
        if(this.towns[t]._id == id){
          notFinded = false;
          return this.towns[t].name;

        }
      }
    }

    if(notFinded){
      return '';
    }
  }

  goBack() { this.navCtrl.pop(); }

  goArgomento(a) {
    let argument;
    for(let i in this.arguments)
    {
      if( this.arguments[i]._id == a )
        argument = this.arguments[i];
    }

    this.navCtrl.push( 'ArgumentPage', { arg: argument } );
  }

  goComune(t) {
    let town;
    for(let i in this.towns)
    {
      if( this.towns[i]._id == t )
        town = this.towns[i];
    }

    this.navCtrl.push( 'TownPage', { town: town } );
  }

  openInBrowser(lat, lng) {
    this.iab.create('https://www.google.com/maps/search/?api=1&query=' + lat + ',' + lng);
  }


}
