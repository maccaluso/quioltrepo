import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the TownsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-towns-list',
  templateUrl: 'towns-list.html',
})
export class TownsListPage {
  towns: any[];
  errorMessage: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider,public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.getTowns();

  }

  getTowns() {
    let loaddd = this.loadingCtrl.create();
	 loaddd.present();
    this.data.getTowns()
      .subscribe(
         (townList) => {
           loaddd.dismiss();
           this.towns = townList;
         },
         error =>  this.errorMessage = <any>error
      );
  }

    goBack() { this.navCtrl.pop(); }

    goComune(t) {
      let town;
      for(let i in this.towns)
      {
        if( this.towns[i]._id == t )
          town = this.towns[i];
      }

      this.navCtrl.push( 'TownPage', { town: town } );
    }

}
