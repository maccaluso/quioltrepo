import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class LocationService {
  public gpsActive:boolean;
  public coords: any;
  constructor(private geolocation: Geolocation) {


    this.geolocation.getCurrentPosition().then((resp) => {
     // resp.coords.latitude
     // resp.coords.longitude
     this.coords = resp.coords;
     this.gpsActive = true;
     console.log('Gps Active');
    }).catch((error) => {
      console.log('Error getting location', error);
      this.gpsActive = false;
       console.log('Gps Inactive');
    });
  }

  getUserLatLng(){
    var pos = {};
    this.geolocation.getCurrentPosition().then((resp) => {
     // resp.coords.latitude
     // resp.coords.longitude
     pos = resp.coords;
     console.log(pos);
     return pos;
    }).catch((error) => {});
  }

}
