import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';

@Injectable()
export class ConnectivityProvider {
 public online:boolean = true;

  constructor(public network: Network,public platform: Platform){
//    this.onDevice = this.platform.is('cordova');
console.log('Hello DataProvider Provider');

    this.platform.ready().then(() => {
      let type = this.network.type;

      //console.log("Connection type: ", this.network.type);
      // Try and find out the current online status of the device
      if(type == "unknown" || type == "none" || type == undefined){
        //console.log("The device is not online");
        this.online = false;
      }else{
        //console.log("The device is online!");
        this.online = true;
      }
    });
  }

  isOnline(){
    console.log(this.online)
  }


}
