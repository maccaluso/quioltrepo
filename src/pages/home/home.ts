import { Component } from '@angular/core';
import { NavController, NavParams, ModalController,ViewController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { LocationService } from '../../providers/location/location';
import { HaversineService, GeoCoord } from "ng2-haversine";
import { Geolocation } from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Diagnostic } from '@ionic-native/diagnostic';
import { TranslateService } from '@ngx-translate/core';
import { App } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
//import { ConnectivityProvider } from '../../providers/connectivity/connectivity';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	arguments: any[];
	towns: any[];
	errorMessage: string;
	userPosition: any;
	mapPlaceholder: any;
	filter: string;
	location2: any;
	sortByDist: boolean = false;
	filterHome: string ;
	lang: string;
	loaddd: any;
	constructor(translate: TranslateService,public loadingCtrl: LoadingController,public navCtrl: NavController,private alertCtrl: AlertController, public navParams: NavParams, public data: DataProvider,public modalCtrl: ModalController,private storage: Storage,private diagnostic: Diagnostic,private geolocation: Geolocation,public location:LocationService,private _haversineService: HaversineService) {
		/*this.gpsActive = false;
		this.connActive = false;*/
		this.mapPlaceholder = 'assets/imgs/map-placeholder.png';
		this.sortByDist = navParams.get('sortByDist');

		this.filter = navParams.get('page');
		this.filterHome = 'name';
		translate.onLangChange.subscribe(event => {
			this.lang = event.lang;
	});

	}

	openModal() {

		let modal = this.modalCtrl.create(ModalContentPage);
		modal.onDidDismiss(data => {
			if(data != null){
				this.navCtrl.push('ItineraryPage', { route: data });
			}

});

		modal.present();
	}

	ionViewWillEnter(){
		setTimeout(() => {
			this.geolocation.getCurrentPosition().then((resp) => {
			 // resp.coords.latitude
			 // resp.coords.longitude
			 this.location2 = resp.coords;
			 console.log(this.location2);
			}).catch((error) => {});

		  }, 0);
			// this.lang = this.translate.currentLang;


	}

	segmentChanged(event){
		console.log(this.location2)
		if(this.location2 == null && event._value == 'distanceUser'){
			let alert = this.alertCtrl.create({
		    title: 'Gps non attivo o app non autorizzata',
		    subTitle: 'Non è possibile ordinare gli argomenti per distanza. Attiva il GPS o autorizza la app Quioltrepo.',
		    buttons: [{
					text: 'OK',
					handler: () => {
                this.filterHome = 'name';
              }
				}]
		  });
		  alert.present();

		}
	}

	ionViewDidLoad() {

		this.getArguments();
		this.getTowns();
		// sessionStorage.setItem('modalShown', 'value');
		var modalShown = sessionStorage.getItem('modalShown');
		console.log(modalShown)
		if(modalShown == null){
			this.openModal();
			sessionStorage.setItem('modalShown', 'true');
		}


		//this.isOnline();
	}


	getArguments() {
		let loaddd = this.loadingCtrl.create();
	 loaddd.present();
		this.data.getArguments()
			.subscribe(
				 (argumentList) => {

					this.arguments = argumentList;

					if( this.filter && this.filter != 'home' )
					{
						this.arguments = this.filterItems( this.filter );
					}
					if(this.location2 != null){
						console.log('posizione ok')
						// Se è inizializzato l'oggetto con coordinate utente aggiungo la distanza all'oggetto argomento

						var pos = {
								latitude: this.location2.latitude,
								longitude: this.location2.longitude
						}

						var obj = {};
						for (let i in this.arguments){
							if(this.arguments[i].location != null && this.arguments[i].location.location != null){
								let posArg: GeoCoord = {
										latitude: this.arguments[i].location.location.geo[1],
										longitude: this.arguments[i].location.location.geo[0]
								}
								if(posArg.latitude != null && posArg.longitude != null){

									obj = {
									"distanceUser" : this._haversineService.getDistanceInMeters(pos,posArg)
									};
									 this.arguments[i] = Object.assign( this.arguments[i], obj);
								}
								else {
									obj = {
									"distanceUser" : 9999999
									};
									 this.arguments[i] = Object.assign( this.arguments[i], obj);

								}
								//this.arguments[i].distanceUser = this._haversineService.getDistanceInMeters(pos,posArg);

							}
							else {
								obj = {
								"distanceUser" : 9999999
								};
								 this.arguments[i] = Object.assign( this.arguments[i], obj);
							}


						}

					}
					else {
						console.log('posizione fallita')
					}
console.log(self)
loaddd.dismiss();
				 },
				 (error) =>  { this.errorMessage = <any>error;loaddd.dismiss(); }
			);
	}

	filterItems(searchTerm){
		return this.arguments.filter((item) => {
			if(item.categories[0])
				return item.categories[0].key.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
		});
	}

	getTowns() {
		this.data.getTowns()
			.subscribe(
				 townList => this.towns = townList,
				 error =>  this.errorMessage = <any>error
			);
	}


	goArgomento(a) {
		// let argument;
		// for(let i in this.arguments)
		// {
		// 	if( this.arguments[i]._id == a )
		// 		argument = this.arguments[i];
		// }

		// this.navCtrl.push( 'ArgumentPage', { arg: argument } );
		this.navCtrl.push( 'ArgumentPage', { arg: a } );
	}

	goItin() {

		this.navCtrl.push( 'ItineraryPage' );
	}

	goRoutes() {

		this.navCtrl.push( 'RoutesPage' );
	}

	goComune(t) {
		let town;
		for(let i in this.towns)
		{
			if( this.towns[i]._id == t )
				town = this.towns[i];
		}

		this.navCtrl.push( 'TownPage', { town: town } );
	}

}

@Component({
	selector: 'modal-import',
  templateUrl: 'modalImport.html'
})
export class ModalContentPage {
	routeCode: any;
	routes: any;
	errorMessage: any;
  constructor(
		public navCtrl: NavController,
    public params: NavParams,
    public viewCtrl: ViewController,
		public data: DataProvider,
		private alertCtrl: AlertController,
		public app: App,
		public loadingCtrl: LoadingController
  ) {

  }

	ionViewDidLoad() {
		this.getRoutes()
	}

		goItin() {
			let loaddd = this.loadingCtrl.create();
		 loaddd.present();
			this.data.getRoutes(this.routeCode)
	      .subscribe(
	        route => {
								 loaddd.dismiss();
	              console.log(route)
	              this.viewCtrl.dismiss(route);

	              // nav.push('ItineraryPage', { route: route });
	              // this.navCtrl.push( 'ItineraryPage', { route: route } );


	        },
	        error =>  this.presentAlert()
	      );
// console.log( this.routes)
// 		let routeFind = false;
//
// 			if( this.routes.appCode!= null && this.routes.appCode == this.routeCode ){
// 					routeFind = true;
// 					this.navCtrl.push( 'ItineraryPage', { route: this.routes } );
// 			}
//
//
// 		if(routeFind === false){
// 			this.presentAlert();
// 		}

	}
  dismiss() {
    this.viewCtrl.dismiss();
  }

	getRoutes() {
		// this.data.getRoutes()
		// 	.subscribe(
		// 		 routesList => this.routes = routesList,
		// 		 error =>  this.errorMessage = <any>error
		// 	);
		// 	console.log('routes')
	}
	presentAlert() {
	  let alert = this.alertCtrl.create({
	    title: 'Itinerario non trovato',
	    subTitle: 'Non risultano itinerari con questo codice',
	    buttons: ['OK']
	  });
	  alert.present();
	}
	goRoutes() {

		this.navCtrl.push( 'RoutesPage' );
	}

}
