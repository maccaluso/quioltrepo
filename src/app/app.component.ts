import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { Geolocation } from '@ionic-native/geolocation';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { ComeFunzionaPage } from '../pages/come-funziona/come-funziona';
import { ArgumentPage } from '../pages/argument/argument';
import { TownsListPage } from '../pages/towns-list/towns-list';
import { TranslateService } from '@ngx-translate/core';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('content') nav: NavController;
  public rootPage:any = HomePage;
  public coords: any;
  public gpsActive : boolean
	public connActive: boolean;
  pages: any[] = [
    { title: 'home', class: 'standard',component:HomePage },
    // { title: 'miei itinerari', class: 'itinerari' },
    { title: 'territorio', class: 'territorio' },
    { title: 'musei', class: 'musei' },
    { title: 'gastronomia', class: 'gastronomia' },
    { title: 'arte', class: 'arte' },
    { title: 'etnografia', class: 'etnografia' },
    { title: 'comuni', class: 'standard',component:TownsListPage },
    { title: 'about', class: 'standard',component:AboutPage },
    { title: 'come funziona', class: 'standard',component:ComeFunzionaPage },
     { title: 'italiano', class: 'standard',language: 'it' },
     { title: 'english', class: 'standard',language: 'en' },
    // { title: 'about', class: 'standard' }
  ]

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private translate: TranslateService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.initTranslate();
    /*this.geolocation.getCurrentPosition().then((resp) => {
     // resp.coords.latitude
     // resp.coords.longitude
     this.coords = resp.coords;
     this.gpsActive = true;
     //console.log(this.coords);
    }).catch((error) => {
      console.log('Error getting location', error);
      this.gpsActive = false;
    });*/
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('it');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('it'); // Set your language here
    }


  }


  openPage(p) {

    if(p.component != null){
      this.nav.push(p.component);
    }else if(p.component == null && p.language != null){
      console.log(p.language);
      this.translate.use(p.language);

    }
    else {
      this.nav.push(HomePage, { page: p.class });
    }




  }





}
