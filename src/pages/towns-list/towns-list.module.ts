import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TownsListPage } from './towns-list';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';


@NgModule({
  declarations: [
    TownsListPage,
  ],
  imports: [
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'itinerarioltrepomantovano' } as CloudinaryConfiguration),
    IonicPageModule.forChild(TownsListPage),
  ],
})
export class TownsListPageModule {}
