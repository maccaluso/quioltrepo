import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { ItineraryPage } from './itinerary';
// import { ModalItineraryPage } from './itinerary';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    ItineraryPage
  ],
  imports: [
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'itinerarioltrepomantovano' } as CloudinaryConfiguration),
    IonicPageModule.forChild(ItineraryPage),
    TranslateModule
  ]
})
export class ItineraryPageModule {}
