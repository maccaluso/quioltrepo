import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComeFunzionaPage } from './come-funziona';

@NgModule({
  declarations: [
    ComeFunzionaPage,
  ],
  imports: [
    IonicPageModule.forChild(ComeFunzionaPage),
  ],
})
export class ComeFunzionaPageModule {}
