var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
/**
 * Generated class for the ArgumentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ArgumentPage = /** @class */ (function () {
    function ArgumentPage(navCtrl, navParams, data) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.data = data;
        this.blocks = Array();
        this.item = navParams.get('arg') || {};
        this.blocks.push(this.item.block1);
        this.blocks.push(this.item.block2);
        this.blocks.push(this.item.block3);
        this.blocks.push(this.item.block4);
        this.blocks.push(this.item.block5);
    }
    ArgumentPage.prototype.ionViewDidLoad = function () {
        this.getArguments();
        this.getTowns();
    };
    ArgumentPage.prototype.getArguments = function () {
        var _this = this;
        this.data.getArguments()
            .subscribe(function (argumentList) { return _this.arguments = argumentList; }, function (error) { return _this.errorMessage = error; });
    };
    ArgumentPage.prototype.getTowns = function () {
        var _this = this;
        this.data.getTowns()
            .subscribe(function (townList) { return _this.towns = townList; }, function (error) { return _this.errorMessage = error; });
    };
    ArgumentPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    ArgumentPage.prototype.goArgomento = function (a) {
        var argument;
        for (var i in this.arguments) {
            if (this.arguments[i]._id == a)
                argument = this.arguments[i];
        }
        this.navCtrl.push('ArgumentPage', { arg: argument });
    };
    ArgumentPage.prototype.goComune = function (t) {
        var town;
        for (var i in this.towns) {
            if (this.towns[i]._id == t)
                town = this.towns[i];
        }
        this.navCtrl.push('TownPage', { town: town });
    };
    ArgumentPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-argument',
            templateUrl: 'argument.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, DataProvider])
    ], ArgumentPage);
    return ArgumentPage;
}());
export { ArgumentPage };
//# sourceMappingURL=argument.js.map
