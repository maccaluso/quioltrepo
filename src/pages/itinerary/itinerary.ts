import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ViewController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { DataProvider } from '../../providers/data/data';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
/**
 * Generated class for the ItineraryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-itinerary',
  templateUrl: 'itinerary.html',
})
export class ItineraryPage {

  arguments: any[];
  routes: any[];
  errorMessage: string;
  route: any;
  item: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public data: DataProvider,public modalCtrl: ModalController, private iab: InAppBrowser,private storage: Storage,private translate: TranslateService) {
    this.item = navParams.get('route') || {};

  }

  openModal() {

    let modal = this.modalCtrl.create(ModalItineraryPage, {item: this.item});
    modal.onDidDismiss(data => {
      if(data){
this.navCtrl.push( 'RoutesPage');
      }

});

    modal.present();
  }

  // ionViewWillEnter(){
  //
	// 	this.geolocation.getCurrentPosition().then((resp) => {
	// 	 // resp.coords.latitude
	// 	 // resp.coords.longitude
	// 	 this.location2 = resp.coords;
	// 	 console.log(this.location2);
	// 	}).catch((error) => {});
  //
	// }

  saveItin(){
    let routeStorage;
    this.storage.get('routes').then((val) => {
      routeStorage = val;
      console.log(routeStorage)
      if(routeStorage != null){
        routeStorage.push(this.item);
        console.log('Add',routeStorage)
        this.storage.set('routes',routeStorage)
      }else {
        routeStorage = [this.item]
        console.log('Create',routeStorage)
        this.storage.set('routes',routeStorage)
      }
    });

  }

  ionViewDidLoad() {
    this.getArguments();
    console.log('Itinerary entered')
  }

  viewWillLeave(){

  }

  openInBrowser() {
    console.log(this.item.arguments[0].location)

    var url = "https://www.google.com/maps/dir/?api=1&";
    var argLength = this.item.arguments.length - 1;
    // destination=45.051233,11.208074&travelmode=driving&waypoints=44.995029,11.072955|45.051233,11.208074|
    url += "origin="+this.item.arguments[0].location.location.geo[1]+","+this.item.arguments[0].location.location.geo[0]+"&destination="+this.item.arguments[argLength].location.location.geo[1]+","+this.item.arguments[argLength].location.location.geo[0]+"&travelmode=driving&waypoints=";

    for(let i = 0;i< this.item.arguments;i++){
      console.log(i)
      if(i!=0 && i!=this.item.arguments.length -1){
        url += this.item.arguments[i].location.location.geo[1]+","+this.item.arguments[i].location.location.geo[0]+"|";
      }


    }
    // console.log('https://www.google.com/maps/dir/?api=1&origin=45.0424799,10.9284632&destination=44.9503008,10.8027268&travelmode=driving&waypoints=45.0424799,10.9284632|44.9316,10.911541|44.9503008,10.8027268|')
    // console.log(url)
    this.iab.create(url);
  }

  getArguments() {
    this.data.getArguments()
      .subscribe(
         argumentList => this.arguments = argumentList,
         error =>  this.errorMessage = <any>error
      );
  }

  getCatClass(id){
    if(id == '597e6b920ee14d81e8ef2f58'){
      this.translate.get('gastronomia').subscribe((res: string) => {
          return res;
      });

    }else if(id == '597e710544abd28248581be5'){
      this.translate.get('arte').subscribe((res: string) => {
          return res;
      });

    }else if(id = '597f22e5e108bc8c799718a5'){
      this.translate.get('territorio').subscribe((res: string) => {
          return res;
      });
    }else if(id = '597f22bde108bc8c799718a4'){
      this.translate.get('musei').subscribe((res: string) => {
          return res;
      });
    }else if(id = '597e6bb40ee14d81e8ef2f59'){
      this.translate.get('etnografia').subscribe((res: string) => {
          return res;
      });
    }
    else {
      return '';
    }
  }

  getCatName(id){
    if(id == '597e6b920ee14d81e8ef2f58'){
      this.translate.get('gastronomia').subscribe((res: string) => {
          return res;
      });

    }else if(id == '597e710544abd28248581be5'){
      this.translate.get('arte').subscribe((res: string) => {
          return res;
      });

    }else if(id = '597f22e5e108bc8c799718a5'){
      this.translate.get('territorio').subscribe((res: string) => {
          return res;
      });
    }else if(id = '597f22bde108bc8c799718a4'){
      this.translate.get('musei').subscribe((res: string) => {
          return res;
      });
    }else if(id = '597e6bb40ee14d81e8ef2f59'){
      this.translate.get('etnografia').subscribe((res: string) => {
          return res;
      });
    }
    else {
      return '';
    }
  }

  goArgomento(a) {
		let argument;
		for(let i in this.arguments)
		{
			if( this.arguments[i]._id == a )
				argument = this.arguments[i];
		}

		this.navCtrl.push( 'ArgumentPage', { arg: argument } );
	}

  reorderItems(indexes) {
    console.log(indexes);
    let element = this.item.arguments[indexes.from];
    this.item.arguments.splice(indexes.from, 1);
    this.item.arguments.splice(indexes.to, 0, element);

    console.log(this.item.arguments);
  }

  getRoutes() {
    this.data.getArguments()
      .subscribe(
         routesList => this.routes = routesList,
         error =>  this.errorMessage = <any>error
      );
  }

  goRoutes(){
    this.navCtrl.push( 'RoutesPage');
  }



  goBack() { this.navCtrl.pop(); }

}

@Component({
	selector: 'modal-itinerary',
  templateUrl: 'modal.html'
})
export class ModalItineraryPage {
  itinName: any;
  item:any;
  constructor(public navParams: NavParams,public viewCtrl: ViewController,private storage: Storage,public navCtrl: NavController,) {
    this.item = navParams.get('item') || {};
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  saveItin(){
    console.log('uuuuuuuuu');
    // if(this.itinName != ''){
    //   this.item.name = this.itinName;
    // }
    // else
    // {
    //   this.item.name = 'Test';
    // }
    let routeStorage;
    this.storage.get('routes').then((val) => {
      routeStorage = val;
      console.log(routeStorage)
      if(routeStorage != null){
        routeStorage.push(this.item);
        console.log('Add',routeStorage)
        this.storage.set('routes',routeStorage)
      }else {
        routeStorage = [this.item]
        console.log('Create',routeStorage)
        this.storage.set('routes',routeStorage)
      }
      this.viewCtrl.dismiss(true);

    });

  }
}
