import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ModalContentPage } from '../pages/home/home';
import { ModalItineraryPage } from '../pages/itinerary/itinerary';
import { ModalRoutesPage } from '../pages/routes/routes';
import { HaversineService } from "ng2-haversine";
import { DataProvider } from '../providers/data/data';
import { LocationService } from '../providers/location/location';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { SortDistancePipe } from '../providers/pipes/sort.pipe';
import { FilterArgPipe } from '../providers/pipes/filterArg.pipe';
import { IonicStorageModule } from '@ionic/storage';
import { AboutPage } from '../pages/about/about';
import { ComeFunzionaPage } from '../pages/come-funziona/come-funziona';
import { ArgumentPage } from '../pages/argument/argument';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TownsListPage } from '../pages/towns-list/towns-list';
import { Diagnostic } from '@ionic-native/diagnostic';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Keyboard } from '@ionic-native/keyboard';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    HomePage,
ModalContentPage,
ModalItineraryPage,
ModalRoutesPage,
SortDistancePipe,
FilterArgPipe,
AboutPage,
ComeFunzionaPage,
TownsListPage
  ],
  imports: [
    BrowserModule,
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'itinerarioltrepomantovano' } as CloudinaryConfiguration),
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {
      pageTransition: 'ios-transition',scrollAssist: false, autoFocusAssist: false
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ModalContentPage,
    ModalItineraryPage,
    ModalRoutesPage,
    AboutPage,
    ComeFunzionaPage,
    TownsListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser,
    DataProvider,
    Geolocation,
    LocationService,
    ConnectivityProvider,
    HaversineService,
    Diagnostic,
    Keyboard
  ]
})
export class AppModule {}
