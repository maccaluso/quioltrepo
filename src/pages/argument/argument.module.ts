import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';
import { TranslateModule } from '@ngx-translate/core';
import { ArgumentPage } from './argument';

@NgModule({
  declarations: [
    ArgumentPage,
  ],
  imports: [
  	CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'itinerarioltrepomantovano' } as CloudinaryConfiguration),
    IonicPageModule.forChild(ArgumentPage),
    TranslateModule,
  ],
})
export class ArgumentPageModule {}
