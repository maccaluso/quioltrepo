import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ViewController} from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { Storage } from '@ionic/storage';
import { HomePage } from '../../pages/home/home';
import { TranslateService } from '@ngx-translate/core';
import { Keyboard } from '@ionic-native/keyboard';
import { Platform } from 'ionic-angular';
import { App } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the RoutesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-routes',
  templateUrl: 'routes.html',
})
export class RoutesPage {

  arguments: any[];
  errorMessage: string;
  routes: any[];

  constructor(public platform: Platform,public navCtrl: NavController, public navParams: NavParams, public data: DataProvider,private storage: Storage,public modalCtrl: ModalController,private translate: TranslateService,private keyboard: Keyboard) {

  }


  openModal() {
		let modal = this.modalCtrl.create(ModalRoutesPage);
    modal.onDidDismiss(data => {
      if(data != null){
        this.navCtrl.push('ItineraryPage', { route: data });
      }

});
		modal.present();

	}

  ionViewDidLoad() {
    this.getArguments();
    this.getRoutes()
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(true);
    });
  }

  ionViewWillLeave() {
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(false);
    });
}

  getArguments() {
    this.data.getArguments()
      .subscribe(
         argumentList => this.arguments = argumentList,
         error =>  this.errorMessage = <any>error
      );
  }

  getRoutes() {
    this.storage.get('routes').then((val) => {
        // console.log(val)
      this.routes = val;
    });

  }

  goItin(r) {
        this.navCtrl.push( 'ItineraryPage', { route: r } );
}

  goBack() { this.navCtrl.push(HomePage); }

}

@Component({
	selector: 'modal-import-routes',
  templateUrl: 'modal.html'
})
export class ModalRoutesPage {
	routeCode: any;
	routes: any;
	errorMessage: any;
  constructor(
		public navCtrl: NavController,
    public params: NavParams,
    public viewCtrl: ViewController,
		public data: DataProvider,
		private alertCtrl: AlertController,
    public platform: Platform,
    private keyboard: Keyboard,
    public app: App,
    public loadingCtrl: LoadingController
  ) {

  }

	ionViewDidLoad() {
		this.getRoutes();
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(true);
    });
	}

  ionViewWillLeave() {
    this.platform.ready().then(() => {
      this.keyboard.disableScroll(false);
    });
}

	goItin() {
		let routeFind = false;

		if( this.routes.appCode == this.routeCode ) {
				routeFind = true;
				this.navCtrl.push( 'ItineraryPage', { route: this.routes } );
		}

		if(routeFind === false) {
			this.presentAlert();
		}

	}

  dismiss() {
    this.viewCtrl.dismiss();
  }

	getRoutes() {
		// this.data.getRoutes()
		// 	.subscribe(
		// 		 routesList => this.routes = routesList,
		// 		 error =>  this.errorMessage = <any>error
		// 	);
		// 	console.log('routes')
	}

  testRoute() {
    let loaddd = this.loadingCtrl.create();
	 loaddd.present();
   console.log('yyyyyyy')
    this.data.getRoutes(this.routeCode)
      .subscribe(
        route => {
            if(route) {
              console.log(route)
              // this.viewCtrl.dismiss();
              // let nav = this.app.getRootNav();
              loaddd.dismiss();
              this.viewCtrl.dismiss(route);
              // let nav = this.app.getRootNav();
              // nav.push('ItineraryPage', { route: route });
              // this.navCtrl.push( 'ItineraryPage', { route: route } );

            }
            else
            {
              loaddd.dismiss()
              this.presentAlert();
            }
        },
        error =>  this.errorMessage = <any>error
      );
      console.log('routes')
  }

	presentAlert() {
	  let alert = this.alertCtrl.create({
	    title: 'Itinerario non trovato',
	    subTitle: 'Non risultano itinerari con questo codice',
	    buttons: ['OK']
	  });
	  alert.present();
	}

	goRoutes() {
		this.navCtrl.push( 'RoutesPage' );
	}

}
